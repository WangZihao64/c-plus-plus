#include<iostream>
using namespace std;
void func(int& ref)
{
    ref=100; //ref是引用，转换为*ref=100
}
typedef struct{
  int a;
  char b;
  short c;
  short d;
}AA_t;
int main()
{
    //引用的本质是指针常量
    //指针常量就是int * const p=&a,p一旦指向了以后，就不可以在指向别的位置了，但是*p赋值这个操作是可以的
    int a=10;
    int& ref=a;
    
    ref=20;  //内部发现ref是引用，自动帮我们转换为*ref=20
    cout << "ref=" << ref << endl;
    cout << "a=" << a << endl;
    func(a);
    return 0;
}
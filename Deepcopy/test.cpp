#include <iostream>
using namespace std;
class Person
{
    public:
    Person()
    {
        cout << "无参构造函数的调用" << endl;
    }
    Person(int age,int height)
    {
        m_age=age;
        m_height=new int(height); 
        cout << "有参构造函数的调用" << endl;
    }
    Person(const Person& p)
    {
        cout << "拷贝构造函数的调用" << endl;
        m_age=p.m_age;
        // m_height=p.m_height; 浅拷贝，编译器默认实现这行代码
        m_height=new int(*p.m_height); //深拷贝
    }
    ~Person()
    {
        if(m_height!=NULL)
        {
        delete m_height;
        m_height=NULL;
        }
        cout << "析构函数的调用" << endl;
    }
    int m_age;
    int *m_height;
};
int main()
{
    //Person p1;
    Person p (18,160);
    cout << "年龄是：  " << p.m_age << "身高是:  " << *p.m_height << endl;
    Person p1 (p);
    cout << "年龄是：  " << p1.m_age << "身高是:  " << *p1.m_height << endl;
    return 0;
}
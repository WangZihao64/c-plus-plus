//
// Created by 王子豪 on 2023/2/6.
//
#include "date.h"
int Date::GetMonthDay(int year, int month)
{
    int month_array[13]={0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0))
    {
        return month_array[2]+1;
    }
    else
    {
        return month_array[month];
    }
}
Date::Date(int year, int month, int day)
{
    if(month>0&&month<13&&day<= GetMonthDay(year,month))
    {
        _year=year;
        _month=month;
        _day=day;
    }
    else
    {
        cout << "输入错误" << endl;
    }
}
void Date::Print()
{
    cout << _year << "/" << _month << "/" << _day << endl;
}
bool Date::operator==(const Date& d)
{
    return _year==d._year&&_month==d._month&&_day==d._day;
}
bool Date::operator!=(const Date& d)
{
    return !(*this==d);
}
//为了支持连续赋值
Date& Date::operator=(const Date& d)
{
    if(this!=&d)
    {
        _year=d._year;
        _month=d._month;
        _day=d._day;
    }
    return *this;
}
bool Date::operator<(const Date& d)
{
    if(_year<d._year)
    {
        return true;
    }
    else if(_year==d._year&&_month<d._month)
    {
        return true;
    }
    else if(_year==d._year&&_month==d._month&&_day<d._day)
    {
        return true;
    }
    return false;
}
bool Date::operator<=(const Date& d)
{
    return *this<d||*this==d;
}
bool Date::operator>(const Date& d)
{
    return !(*this<=d);
}
bool Date::operator>=(const Date& d)
{
    return !(*this<d);
}
Date& Date::operator+=(int day)
{
    _day+=day;
    while(_day> GetMonthDay(_year,_month))
    {
        _day-= GetMonthDay(_year,_month);
        _month++;
        if(_month>12)
        {
            _year++;
            _month=1;
        }
    }
    return *this;
}
//不能用引用返回，tmp出了作用域就销毁了
Date Date::operator+(int day)
{
    Date tmp=*this;
    tmp+=day;
    return tmp;
}
//前置++
Date& Date::operator++()
{
    *this+=1;
    return *this;
}
//后置++
Date Date::operator++(int)
{
    Date tmp=*this;
    *this+=1;
    return tmp;
}
Date& Date::operator-=(int day)
{
    _day-=day;
    while(_day<=0)
    {
        --_month;
        if(_month==0)
        {
            --_year;
            _month=12;
        }
        _day+= GetMonthDay(_year,_month);
    }
    return *this;
}
Date Date::operator-(int day)
{
    Date tmp=*this;
    tmp-=day;
    return tmp;
}
Date& Date::operator--()
{
    *this-=1;
    return *this;
}
Date& Date::operator--(int)
{
    Date tmp=*this;
    *this-=1;
    return tmp;
}
int Date::operator-(Date& d)
{
    Date max=*this;
    Date min=d;
    int flag=1;
    if(d>(*this))
    {
        max=d;
        min=*this;
        flag=-1;
    }
    int res=0;
    while(min!=max)
    {
        min++;
        res++;
    }
    return res*flag;
}
#include<iostream>
using namespace std;
class Person
{
    friend ostream & operator<<(ostream &cout,Person& p);
    friend void test01();
    // friend Person operator+(Person& p1,Person& p2);
    public:
    //成员函数重载加号
    // Person operator+(Person &p)
    // {
    //     Person tmp;
    //     tmp.m_a=this->m_a+p.m_a;
    //     tmp.m_b=this->m_b+p.m_b;
    //     return tmp;
    // }
    private:
    int m_a;
    int m_b;
};
// 全局函数重载加号
// Person operator+(Person& p1,Person& p2)
// {
//     Person tmp;
//     tmp.m_a=p1.m_a+p2.m_a;
//     tmp.m_b=p1.m_b+p2.m_b;
//     return tmp;
// }
//只能利用全局函数重载左移运算符
ostream & operator<<(ostream &cout,Person &p)
{
    cout << "m_a = " << p.m_a << endl;;
    cout << "m_b = " << p.m_b << endl;
    return cout;
}
void test01()
{
    Person p1;
    p1.m_a=10;
    p1.m_b=10;
    Person p2;
    p2.m_a=20;
    p2.m_b=30;
    Person p3;
    //成员函数调用的本质
    // p3=p1.operator+(p2);
    //全局函数调用的本质
    // p3=operator+(p1,p2);
    //成员函数和全局函数都可以用这个
    // p3=p2+p1;
    // cout << "p1+p2 m_a= " << p3.m_a << endl;
    // cout << "p1+p2 m_b= " << p3.m_b << endl;
    cout << p1 << endl;
}
int main()
{
    test01();
}
#include<iostream>
#include<vector>
using namespace std;
  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };
class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        int max=0;
        int index=0;
        int i=0;
        //创建结点
        TreeNode* newnode=new TreeNode;
        for(i=0;i<nums.size();i++)
        {
            //找到最大元素
            if(nums[i]>nums[max])
            {
                max=i;
                index=i;
            }
        }
        newnode->val=nums[max];
        newnode->left=nullptr;
        newnode->right=nullptr;
        //保证左区间至少有一个值
        if(index>0)
        {
            //左区间创建一个新容器，排出最大的元素后
            vector<int> newleft(nums.begin(),nums.begin()+index);
            newnode->left=constructMaximumBinaryTree(newleft);
        }
        //保证右区间至少有一个值
        if(index<nums.size()-1)
        {
            //右区间创建一个新容器，排出最大的元素后
            vector<int> newright(nums.begin()+index+1,nums.end());
            newnode->right=constructMaximumBinaryTree(newright);
        }
        return newnode;
    }
};
int main()
{
    vector<int> v1{3,6,0,5};
    Solution p1;
    p1.constructMaximumBinaryTree(v1);
    
    return 0;
}

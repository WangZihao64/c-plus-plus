//
// Created by 王子豪 on 2023/2/6.
//

#ifndef DATE_DATE_H
#define DATE_DATE_H

#endif //DATE_DATE_H
#pragma once
#include<iostream>
using namespace std;
class Date{
public:
    //默认构造
    //全缺省
    Date(int year=2023, int month=2, int day=6);
    void Print();
    //运算符重载
    bool operator==(const Date& d);
    bool operator!=(const Date& d);
    bool operator<(const Date& d);
    bool operator<=(const Date& d);
    bool operator>(const Date& d);
    bool operator>=(const Date& d);
    Date& operator+=(int day);
    Date operator+(int day);
    Date& operator++();
    Date operator++(int);
    Date operator-(int day);
    Date& operator-=(int day);
    //赋值重载
    Date& operator=(const Date& d);
    Date& operator--();
    Date& operator--(int);
    int operator-(Date& d);
    //获得每月的天数
    int GetMonthDay(int year, int month);
private:
    int _year;
    int _month;
    int _day;
};

#pragma once
#include"point.h"

class Circle
{
    //设置半径
    //设置圆心
    public:
    void setR(int r);
    int getR();
    void setCentre(Point centre);
    Point getCentre();
    private:
    int m_r;
    Point m_centre;
};
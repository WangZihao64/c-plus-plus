#include<iostream>
#include<string>
using namespace std;
class CPU
{
    public:
    virtual void calculate()=0;
};
class VideoCard
{
    public:
    virtual void display()=0;
};
class Memory
{
    public: 
    virtual void storage()=0;
};

//具体厂商
class IntelCPU:public CPU
{
    public:
    virtual void calculate()
    {
        cout << "英特尔的CPU正在计算" << endl;
    }
};
class IntelVideoCard:public VideoCard
{
    public:
    virtual void display()
    {
        cout << "英特尔的显卡正在显示" << endl;
    }
};
class IntelMemory:public Memory
{
    public:
    virtual void storage()
    {
        cout << "英特尔的内存正在工作" << endl;
    }
};
class Computer
{
    public:
    Computer(CPU* cpu,VideoCard* vc,Memory* mem)
    {
        m_cpu=cpu;
        m_vc=vc;
        m_mem=mem;
    }
    //提供工作的函数
    void work()
    {
        m_cpu->calculate();
        m_vc->display();
        m_mem->storage();
    }
    ~Computer()
    {
        if(m_cpu!=NULL)
        {
            delete m_cpu;
            m_cpu=NULL;
        }
        if(m_vc!=NULL)
        {
            delete m_vc;
            m_vc=NULL;
        }
        if(m_mem!=NULL)
        {
            delete m_mem;
            m_mem=NULL;
        }
    }
    private:
    CPU* m_cpu; //cpu零件的指针
    VideoCard* m_vc;
    Memory* m_mem;
};
void test01()
{
    CPU* intelCPU=new IntelCPU;
    VideoCard* intelvidecard=new IntelVideoCard;
    Memory* intelmemory=new IntelMemory;
    //创建电脑
    Computer* computer1=new Computer(intelCPU,intelvidecard,intelmemory);
    computer1->work();
    delete computer1;
}
int main()
{
    test01();
    return 0;
}
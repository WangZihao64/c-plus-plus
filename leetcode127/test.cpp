//
// Created by 王子豪 on 2023/1/11.
//
#include <iostream>
#include <vector>
#include<unordered_map>
#include<queue>
#include<unordered_set>
using namespace std;
class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        //广度优先只要到达了终点，那么一定是最短路径！
        //放到set里面查找比较方便
        unordered_set<string> word_set(wordList.begin(),wordList.end());
        //最终结果都没有出现在字典中
        if(word_set.find(endWord)==word_set.end())
        {
            return 0;
        }
        //用来存放路径长度
        unordered_map<string,int> word_map;
        word_map.insert(pair<string,int>(beginWord,1));
        //把搜到的单词放到队列里
        queue<string> q;
        q.push(beginWord);
        int path=0;
        while(!q.empty())
        {
            path++;
            string word=q.front();
            q.pop();
            for(int i=0;i<word.size();i++)
            {
                string newword=word;
                for(int j=0;j<26;j++)
                {
                    newword[i]=j+'a';
                    if(newword==endWord)
                    {
                        return word_map[word]+1;
                    }
                    //添加条件为1.新组成的单词必须是字典中的，2.不需要重复添加
                    if(word_set.find(newword)!=word_set.end()&&word_map.find(newword)!=word_map.end())
                    {
                        q.push(newword);
                        word_map.insert(pair<string,int>(newword,path));
                    }
                }
            }
        }
        return 0;
    }
};
int main()
{
    cout << "www ";
    cout <<"w";
    Solution s1;
    string beginWord="hit";
    string endWord="cog";
    vector<string> wordList={"hot","dot","dog","lot","log","cog"};
    int ret=s1.ladderLength(beginWord,endWord,wordList);
    return 0;
}

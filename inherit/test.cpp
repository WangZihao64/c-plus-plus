#include<iostream>
using namespace std;
class Base
{
    public:
    void header()
    {
        cout << "首页--番剧--直播--游戏中心--会员购--漫画--赛事" << endl;
    }
    void footer()
    {
        cout << "国创--综艺--动画--鬼畜--舞蹈--美食--汽车-运动" << endl;
    }
};
class China:public Base
{
    public:
    void content()
    {
        cout << "灵笼--斗破苍穹" << endl;
    }
};
class Cartoon:public Base
{
    public:
    void content()
    {
        cout << "海贼王--火影忍者" << endl;
    }
};
class Base1
{
    public:
    int m_a;
    protected:
    int m_b;
    private:
    int m_c;
};
class test:public Base1
{
    void func()
    {
        m_a=10; //原来是public 到这里还是public
        m_b=10; //原来是protected 到这里还是protected
        //m_c=10; //原来是private 在这里还是private，访问不了
    }
};
void test01()
{
    China p;
    p.header();
    p.footer();
    p.content();
    Cartoon p1;
    p1.header();
    p1.footer();
    p1.content();
}
int main()
{
    test01();
    return 0;
}
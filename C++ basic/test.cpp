#include<iostream>
#include<string>
using namespace std;
//引用传递，形参会修饰实参的
void swap(int &a,int &b)
{
    int tmp=a;
    a=b;
    b=tmp;
}
//不能返回局部变量的引用！错误示范
// int& test()
// {
//     int a=10;
//     return a;
// }
//返回静态变量的引用
int& test2()
{
    static int a=10;
    return a;
}
int main()
{
    // //1.创建bool数据类型
    // bool flag=true;
    // cout << flag << endl;  //1
    // flag=false;
    // cout << flag << endl;  //0
    // //2.查看bool累成所占内存空间
    // cout << "bool类型所占的空间大小:" << sizeof(bool) <<endl; //1


    //cin用来输入
    //字符串类型
    // string str;
    // cout << "请输入字符串:" << endl;
    // cin >> str;
    // cout << "字符串为：" << str << endl;
    //new是在堆区上开辟
    //动态开辟一个数组
    // int* arr=new int[10];
    // int i=0;
    // for(i=0;i<10;i++)
    // {
    //     cin >> arr[i];
    // }
    // for(i=0;i<10;i++)
    // {
    //     cout << arr[i] << " ";
    // }
    //引用：给变量起别名
    //语法：数据类型 &别名=原名
    // int a=10;
    // int &b=a;
    // b=100;
    // cout << a << endl;
    // int a=10;
    // int b=20;
    // swap(a,b);
    // cout << a << endl;
    // cout << b << endl;
    int& ref=test2();
    //ref是别名，test2()返回的是a
    test2()=100;
    cout << ref << endl;
    return 0;
}
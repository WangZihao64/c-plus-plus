#include<iostream>
using namespace std;
//静态成员变量的特点：
//1.在编译阶段分配内存
//2.类内声明，类外初始化
//3.所有对象共用同一份数据
class Person
{
    public:
    static int m_a;
    private:
    static int m_b;
};
int Person::m_a = 100;
int Person::m_b = 300;
void test1()
{
    //静态成员变量有两种访问方式
    //1.通过对象进行访问
    Person p;
    cout << p.m_a << endl;
    Person p2;
    p2.m_a=200;
    cout << p.m_a << endl;
    //2.通过类名访问
    cout << Person::m_a << endl;
    //静态成员也不能用私有的
    // cout << Person::m_b << endl;
}
int main()
{
    test1();
    return 0;
}

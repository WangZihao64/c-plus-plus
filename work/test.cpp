// #include<iostream>
// #include<vector>
// #include<string>
// #include<map>
// #define CEHUA 0
// #define YANFA 1
// #define SUANFA 2
// using namespace std;
// class work
// {
//     public:
//     string m_name;
//     int m_age;
// };
// void creatework(vector<work>& w1)
// {
//     string str="ABCDEFGHIJ";
//     work worker;
//     int i=0;
//     for(i=0;i<10;i++)
//     {
//         worker.m_name="员工";
//         worker.m_name+=str[i];
//         worker.m_age=i+10;
//         w1.push_back(worker);
//     }
// }
// void setGroup(vector<work>& w1,multimap<int,work>& m_worker)
// {
//     for(vector<work>::iterator it=w1.begin();it!=w1.end();it++)
//     {
//         int depID=rand()%3;
//         m_worker.insert(make_pair(depID,*it));
//     }
// }
// void ShowWorkByGroup(multimap<int,work>& m_worker)
// {
//     multimap<int,work>::iterator pos=m_worker.find(CEHUA);
//     int count=m_worker.count(CEHUA);
//     int index=0;
//     for(;index<count;pos++,index++)
//     {
//         cout << pos->second.m_name << endl;
//     }
// }
// void test01()
// {
//     vector<work> w1;
//     creatework(w1);
//     multimap<int,work> m_worker;
//     setGroup(w1,m_worker);
//     ShowWorkByGroup(m_worker);
// }
// int main()
// {
//     test01();
//     return 0;
// }
#include<iostream>
using namespace std;
#include <vector>
#include <string>
#include <map>
#include <ctime>

/*
- 公司今天招聘了10个员工（ABCDEFGHIJ），10名员工进入公司之后，需要指派员工在那个部门工作
- 员工信息有: 姓名  工资组成；部门分为：策划、美术、研发
- 随机给10名员工分配部门和工资
- 通过multimap进行信息的插入  key(部门编号) value(员工)
- 分部门显示员工信息
*/

#define CEHUA  0
#define MEISHU 1
#define YANFA  2

class Worker
{
public:
	string m_Name;
	int m_Salary;
};

void createWorker(vector<Worker>& v)
{
	string nameSeed = "ABCDEFGHIJ";
	for (int i = 0; i < 10; i++)
	{
		Worker worker;
		worker.m_Name = "员工";
		worker.m_Name += nameSeed[i];

		worker.m_Salary = rand() % 10000 + 10000; // 10000 ~ 19999
		//将员工放入到容器中
		v.push_back(worker);
	}
}

//员工分组
void setGroup(vector<Worker>& v, multimap<int, Worker>& m)
{
	for (vector<Worker>::iterator it = v.begin(); it != v.end(); it++)
	{
		//产生随机部门编号
		int deptId = rand() % 3; // 0 1 2 

		//将员工插入到分组中
		//key部门编号，value具体员工
		m.insert(make_pair(deptId, *it));
	}
}

void showWorkerByGourp(multimap<int, Worker>& m)
{
	// 0  A  B  C   1  D  E   2  F G ...
	cout << "策划部门：" << endl;

	multimap<int, Worker>::iterator pos = m.find(CEHUA);
	int count = m.count(CEHUA); // 统计具体人数
	int index = 0;
	for (; pos != m.end() && index < count; pos++, index++)
	{
		cout << "姓名： " << pos->second.m_Name << " 工资： " << pos->second.m_Salary << endl;
	}

	cout << "----------------------" << endl;
	cout << "美术部门： " << endl;
	pos = m.find(MEISHU);
	count = m.count(MEISHU); // 统计具体人数
	index = 0;
	for (; pos != m.end() && index < count; pos++, index++)
	{
		cout << "姓名： " << pos->second.m_Name << " 工资： " << pos->second.m_Salary << endl;
	}

	cout << "----------------------" << endl;
	cout << "研发部门： " << endl;
	pos = m.find(YANFA);
	count = m.count(YANFA); // 统计具体人数
	index = 0;
	for (; pos != m.end() && index < count; pos++, index++)
	{
		cout << "姓名： " << pos->second.m_Name << " 工资： " << pos->second.m_Salary << endl;
	}

}

int main() {

	srand((unsigned int)time(NULL));

	//1、创建员工
	vector<Worker>vWorker;
	createWorker(vWorker);

	//2、员工分组
	multimap<int, Worker>mWorker;
	setGroup(vWorker, mWorker);


	//3、分组显示员工
	showWorkerByGourp(mWorker);

	
	//for (vector<Worker>::iterator it = vWorker.begin(); it != vWorker.end(); it++)
	//{
	//	cout << "姓名： " << it->m_Name << " 工资： " << it->m_Salary << endl;
	//}

	system("pause");

	return 0;
}
/*
策划部门：
姓名： 员工A 工资： 12914
姓名： 员工D 工资： 15027
姓名： 员工F 工资： 16397
姓名： 员工I 工资： 10505
----------------------
美术部门：
姓名： 员工B 工资： 15037
姓名： 员工E 工资： 14595
姓名： 员工G 工资： 12491
姓名： 员工H 工资： 13004
----------------------
研发部门：
姓名： 员工C 工资： 17698
姓名： 员工J 工资： 15016
请按任意键继续. . .
*/


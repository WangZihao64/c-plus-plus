#include<iostream>
using namespace std;
class Animals
{
    public:
    int m_age;
};
//虚继承
class Sheep:virtual public Animals
{

};
class Tuo:virtual public Animals
{

};
class SheepTuo:public Sheep,public Tuo
{

};
void test01()
{
    SheepTuo p;
    //虚继承的时候只会保存一个
    p.Tuo::m_age=10;
    p.Sheep::m_age=20;
    //当菱形继承的时候，两个父类拥有相同的数据，需要加以作用域区分
    cout << "m_age= " << p.Tuo::m_age << endl;
    cout << "m_age= " << p.Sheep::m_age << endl;
}
int main()
{
    test01();
    return 0;
}
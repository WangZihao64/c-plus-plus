#include<iostream>
#include<string>
using namespace std;
// class Person
// {
//     public:
//     //传统构造函数初始化
//     // Person(int a,int b,int c)
//     // {
//     //     m_a=a;
//     //     m_b=b;
//     //     m_c=c;
//     // }
//     //初始化列表初始化属性
//     // Person():m_a(10),m_b(20),m_c(30)
//     // {

//     // }
//     Person(int a, int b, int c):m_a(a),m_b(b),m_c(c)
//     {

//     }
//     int m_a;
//     int m_b;
//     int m_c;
// };
class Phone
{
    public:
    Phone(string brand)
    {
        m_brand=brand;
    }
    string m_brand;
};
class Person
{
    public:
    //Phone m_phone=phone; 隐式转换
    Person(string name,string phone):m_name(name),m_phone(phone)
    {

    }
    string m_name;
    Phone m_phone;
};
// void test1()
// {
//     Person p(10,20,30);
//     // Person p;
//     cout << " m_a= " << p.m_a << " m_b= " << p.m_b << " m_c= " << p.m_c << endl;
// }
void test2()
{
    Person p("张三","IPhone");
    cout << p.m_name << "拿着" << p.m_phone.m_brand << endl;
}
int main()
{
    // test1();
    test2();
    // //c++11的新特性
    // int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };  
    // for (auto n : arr)  
    // {
    //     cout << n << " ";  
    // }
    // cout << endl;
    // for(auto& e : arr)
    // {
    //     e*=2;
    //     cout << e << " ";
    // }
}

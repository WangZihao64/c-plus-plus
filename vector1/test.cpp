#include<iostream>
#include<vector>
using namespace std;
void PrintVector(vector<int>& v)
{
    for(vector<int>::iterator it=v.begin();it!=v.end();it++)
    {
        cout << *it << " ";
    }
    cout << endl;
}
void test01()
{
    //默认构造 无参构造
    vector<int> v1;
    int i=0;
    for(i=0;i<10;i++)
    {
        v1.push_back(i);
    }
    PrintVector(v1);
    //通过区间方式进行构造
    vector<int> v2(v1.begin(),v1.end());
    PrintVector(v2);
    //n个elem进行构造
    vector<int> v3(10,100);
    PrintVector(v3);
    //拷贝构造
    vector<int> v4(v3);
    PrintVector(v4);
    vector<int> v5;
    for(i=0;i<10;i++)
    {
        v5.push_back(i+30);
    }
    //利用括号去访问元素
    for(i=0;i<v5.size();i++)
    {
        cout << v5[i] << " ";
    }
    cout << endl;
    //利用at去访问元素
    for(i=0;i<v5.size();i++)
    {
        cout << v5.at(i) << " ";
    }
    cout << endl;
    //front是第一个元素
    cout << v5.front() << endl;
    //back是最后一个元素
    cout << v5.back() << endl;
}
int main()
{
    test01();
    return 0;
}

#include<string>
#include"point.h"
#include"circle.h"
//c++面向对象三大特性：封装，继承，多态
using namespace std;
#define PI 3.14
//封装的意义：将属性和行为作为一个整体，表现生活中的食物
//将属性和行为加以权限控制
//计算圆的半径
//创建一个类
// class Circle
// {
//     //访问权限
//     public:
//     //属性
//     int m_c;
//     //行为
//     int calculation()
//     {
//         return 2*PI*m_c;
//     }
// };
//设计一个学生类
class Student
{
    public:
    string m_name;
    int m_id;
    void print()
    {
        cout << "学生的姓名为:" << m_name << endl;
        cout << "学生的学号为:" << m_id << endl;
    }
    void Setname(string name)
    {
        m_name=name;
    }
    void Setid(int id)
    {
        m_id=id;
    }
};
//public 类内可以访问 类外可以访问
//protected 类内可以访问 类外不可以访问
//private 类内可以访问 类外不可以访问
//class和struct的唯一区别在于：struct默认权限是公共，class默认权限是私有
// class Person
// {
//     public:
//     string p_name;
//     protected:
//     string p_car;
//     private:
//     int password;
//     public:
//     void func()
//     {
//         p_name="王子豪";
//         p_car="保时捷";
//         password=1234;
//     }
// };
class Person
{
    public:
    //名字可读可写
    void setName(string name)
    {
        p_name=name;
    }
    string getName()
    {
        return p_name;
    }
    //可读
    string getCar()
    {
        p_car="保时捷";
        return p_car;
    }
    //可写
    void setPassword(int pword)
    {
        password=pword;
    }
    bool isSame(Person& s2)
    {
        if(password==s2.password)
        {
            return true;
        }
        return false;
    }
    private:
    string p_name;
    string p_car;
    int password;
};
// class Point
// {
//     public:
//     void setX(int x)
//     {
//         m_x=x;
//     }
//     int getX()
//     {
//         return m_x;
//     }
//     void setY(int y)
//     {
//         m_y=y;
//     }
//     int getY()
//     {
//         return m_y;
//     }
//     private:
//     int m_x;
//     int m_y;
// };
// class Circle
// {
//     //设置半径
//     //设置圆心
//     public:
//     void setR(int r)
//     {
//         m_r=r;
//     }
//     int getR()
//     {
//         return m_r;
//     }
//     void setCentre(Point centre)
//     {
//         m_centre=centre;
//     }
//     Point getCentre()
//     {
//         return m_centre;
//     }
//     private:
//     int m_r;
//     Point m_centre;
// };
//判断点和圆的关系
void isInCirecle(Circle &c,Point &p)
{
    //计算两点之间距离的平方
    int distance=
    (c.getCentre().getX()-p.getX())*(c.getCentre().getX()-p.getX())+
    (c.getCentre().getY()-p.getY())*(c.getCentre().getY()-p.getY());
    //半径的平方
    int rdistance=c.getR()*c.getR();
    //判断关系
    if(distance==rdistance)
    {
        cout << "点在圆上" << endl;
    }
    else if(distance>rdistance)
    {
        cout << "点在圆外" << endl;
    }
    else
    {
        cout << "点在圆内" << endl;
    }
}
int main()
{
    //创建具体的对象
    // Circle c1;
    // c1.m_c=2;
    // cout << c1.calculation() << endl;
    Student s1;
    // s1.m_name="王子豪";
    // s1.m_id=123;
    // s1.Setname("王子豪");
    // s1.Setid(123);
    // s1.print();
    // Person p1;
    // Person p2;
    // p1.setName("王子豪");
    // cout << "名字为:" << p1.getName() << endl;
    // cout << "车型号为:" << p1.getCar() << endl;
    // p1.setPassword(123456);
    // p2.setPassword(123456);
    // bool ret=p1.isSame(p2);
    // cout << ret << endl;
    //创建圆
    Circle c;
    c.setR(10);
    Point centre;
    centre.setX(10);
    centre.setY(0);
    c.setCentre(centre);
    //创建点
    Point p;
    p.setX(10);
    p.setY(9);
    isInCirecle(c,p);
    return 0;
}
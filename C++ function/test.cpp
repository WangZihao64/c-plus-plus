#include<iostream>
using namespace std;
// void test(const int& a)
// {
//     cout << a << endl;
// }
int test1(int a=10,int b=20)
{
    return a+b;
}
//该写法错误，如果某个位置参数有默认值，那么从这个位置往后，都需要有默认值
// int test2(int a,int b=20,int c)
// int test3(int a=10,int b=20);
// //函数声明和函数实现只能有一个有默认参数
// int test3(int a,int b)
// {
//     return a+b;
// }
//函数占位参数，占位参数也可以有默认值
void test4(int a,int = 10)
{
    cout << "hello world" << endl;
}
//函数重载的满足条件
//1.同一个作用域下
//2.函数名称相同
//3.函数的参数类型不同，顺序不同，或者个数不同
// void demo()
// {
//     cout << "demo()的调用" << endl;
// }
// void demo(int a)
// {
//     cout << "demo(int a)的调用" << endl;
// }
// void demo(int a,double b)
// {
//     cout << "demo(int a,double b)的调用" << endl;
// }
// void demo(double a,int b)
// {
//     cout << "demo(double a,int b)的调用" << endl;
// }
//注意事项
//1.函数的返回值不可以作为函数重载的条件（因为两个名字都一样，只是返回类型不一样，编译器不知道该用哪一个）
// int demo(double a,int b)
// {
//     cout << "demo(double a,int b)的调用" << endl;
// }
//2.引用作为重载条件
void demo1(const int& a) 
{
    cout << "demo1(const int& a)的调用" <<endl;
}
void demo1(int& a)
{
    cout << "demo1(int& a)的调用" <<endl;
}
int main()
{
    //int& a=10; 引用本身需要一个合法的内存空间，因此这行错误
    const int& a=10; //加入const就可以了，编译器优化代码，int tmp=10; const int& a=tmp
    //a=100   加入const后不可以修改变量
    // test(a);
    // cout << a << endl;
    //cout << test1() << endl;
    // test4(10);
    int b=10;
    demo1(b); //调用demo1(int& a),因为int& a=b语法正确
    demo1(10); //调用demo1(const int& a),因为const int& a=10语法正确
    return 0;
}
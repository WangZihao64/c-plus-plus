//
// Created by 王子豪 on 2023/7/30.
//
#include"SmartPtr.h"
void test_auto_ptr()
{
    wzh::auto_ptr<int> ptr1(new int);
    wzh::auto_ptr<int> ptr2(ptr1);
    //ptr1被置空了，所以++会造成空指针异常
    (*ptr1)++;
}
void test_unique_ptr()
{
    wzh::unique_ptr<int> ptr1(new int);
    //防止拷贝
//    wzh::unique_ptr<int> ptr2(ptr1);
}
struct ListNode
{
    int val;
    wzh::shared_ptr<ListNode> _prev;
    wzh::shared_ptr<ListNode> _next;
    ~ListNode()
    {
        cout << "~ListNode" <<endl;
    }
};
void test_shared_ptr()
{
    wzh::shared_ptr<int> ptr1(new int(2));
    wzh::shared_ptr<int> ptr2(ptr1);
    ++(*ptr2);
    ++(*ptr1);
}
void test_weak_ptr()
{
//    shared_ptr<ListNode> n1(new ListNode);
//    shared_ptr<ListNode> n2(new ListNode);
    wzh::shared_ptr<ListNode> n1(new ListNode);
    wzh::shared_ptr<ListNode> n2(new ListNode);
    n1->_next=n2;
    n2->_prev=n1;
}
int main()
{
//    test_shared_ptr();
//    test_unique_ptr();
    test_weak_ptr();
}
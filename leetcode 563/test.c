#include<stdio.h>
#include<stdlib.h>
#include<string.h>
class Solution {
public:
    string makeGood(string s) {
        //用string模拟栈
        string result;
        for(int i=0;i<s.size();i++)
        {
            result+=s[i];
            int n=result.size();
            if(n>=2&&result[n-1]-32==result[n-2]||result[n-1]+32==result[n-2])
            {
                result.resize(n-2);
            }
        }
        return result;
    }
};

int main()
{
	char* s="ABbaCc";
	char* ret=makeGood(s);
	return 0;
}



#include<iostream>
#include<string>
using namespace std;
class Person
{
    public:
    int age;
    //构造函数
    //1.没有返回值也不写void
    //2.函数名称与类名相同
    //3.构造函数可以有参数，因此可以发生重载
    //程序在调用对象时会自动调用构造，无需手动调用，而且只会调用一次
    //不写编译器也会创造，不过是空实现
    Person ()
    {
        cout << "无参构造函数的调用" << endl;
    }
    //有参
    Person(int a)
    {
        age=a;
        cout << "有参数构造函数的调用" << endl;
    }
    //拷贝构造（除了拷贝构造，其他都叫普通构造）
    Person(const Person& p)
    {
        //将传人的人的身上的所有属性，拷贝到我身上
        age=p.age;
        cout << "拷贝构造函数的调用" << endl;
    }
    //析构函数 进行清理的操作
    //1.没有返回值 不写void
    //2.函数名和类名相同 在名称前加～
    //3.析构函数不可以有参数，不可以发生重载
    //4.程序在调用对象时会自动调用析构，无需手动调用，而且只会调用一次
    ~Person()
    {
        cout << "析构函数的调用" << endl;
    }
    
};
void test()
{
    //1.括号法 
    // Person p1;
    
    // Person p2(10);
    // Person p3(p2);
    //注意事项
    //调用默认构造函数,不要加（）
    //编译器会认为这是一个函数的声明，不会认为是在创建对象
    // Person p1();
    // 2.显示法
    // Person p1;
    // Person p2=Person(10);
    // Person p3=Person(p2);
    // Person(10);; //匿名对象 特点：当前执行结束后，系统会立刻回收掉匿名对象
    // cout << "aaa" << endl;
    //不要匿名拷贝构造函数，编译器会认为Person p3==Person (p3)
    // Person (p3);
    //隐式转换法
    Person p4=10;
    Person p5=p4;
}
int main()
{
    test();

    return 0;
}
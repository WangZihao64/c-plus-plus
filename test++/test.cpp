// #include<iostream>
// #include<vector>
// #include<unordered_set>
// using namespace std;
// class Solution {
//     vector<vector<int>> result;
//     vector<int> path;
//     void backtracking(vector<int>& nums,int startIndex)
//     {
//         // if(path.size()>1)
//         // {
//         //     result.push_back(path);
//         // }
//         unordered_set<int> uset;
//         for(int i=startIndex;i<nums.size();i++)
//         {
//             if((!path.empty()&&nums[i]<path.back())||uset.find(nums[i]) != uset.end())
//             {
//                 continue;
//             }
//             uset.insert(nums[i]);
//             path.push_back(nums[i]);
//             backtracking(nums,i+1);
//             path.pop_back();
//         }
//     }
// public:
//     vector<vector<int>> findSubsequences(vector<int>& nums) {
//         backtracking(nums,0);
//         return result;
//     }
// };
// int main()
// {
//     Solution s1;
//     vector<int> nums{4,6,7,7};
//     s1.findSubsequences(nums);
    
//     return 0;
// }

#include <iostream>
#include <string>
#include <unordered_set>

int main ()
{
  std::unordered_set<std::string> myset = { "red","green","blue" };

  std::string input;
  std::cout << "color? ";
  input="blue";

  std::unordered_set<std::string>::const_iterator got = myset.find (input);

  if ( got == myset.end() )
    std::cout << "not found in myset";
  else
    std::cout << *got << " is in myset";

  std::cout << std::endl;

  return 0;
}
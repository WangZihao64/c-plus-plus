#include<iostream>
#include<vector>
#include<unordered_set>
#include<string>
#include<unordered_map>
using namespace std;
void test_complepack()
{
    vector<int> weight = {1};
    vector<int> value = {1};
    int bagWeight = 0;
    vector<long> dp(bagWeight + 1, INT_MAX);
    dp[0]=0;
    //遍历物品
    for(int i=0;i<value.size();i++)
    {
        //遍历背包
        //这个写法更好
        //for(int j=weight[i];j<=bagWeight;j++)
        for(int j=0;j<=bagWeight;j++)
        {
            if(j>=weight[i])
            dp[j]=min(dp[j],dp[j-weight[i]]+1);
        }
    }
    cout << dp[bagWeight] << endl;
}
class Solution {
    private:
    int _leftrange(vector<int>&nums,int target)
    {
        int left=0;
        int right=nums.size()-1;
        //记录左边界
        int leftrange=-1;
        while(left<=right)
        {
            //防止越界
            int mid=left+(right-left)/2;
            if(nums[mid]>=target)
            {
                right=mid-1;
                leftrange=mid;
            }
            else
            {
                left=mid+1;
            }
        }
        //返回-1相当于没搜索到
        return leftrange;
    }
    int _rightrange(vector<int>&nums,int target)
    {
        int left=0;
        int right=nums.size()-1;
        //记录左边界
        int rightrange=-1;
        while(left<=right)
        {
            //防止越界
            int mid=left+(right-left)/2;
            if(nums[mid]<=target)
            {
                left=mid+1;
                rightrange=mid;
            }
            else
            {
                right=mid-1;
            }
        }
        return rightrange;
    }
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        //我们需要寻找右边界，左边界
        int left=_leftrange(nums,target);
        int right=_rightrange(nums,target);
        //情况一,值超出边界
        if(left==-1||right==-1)
        {
            return {-1,-1};
        }
        else 
        {
            return {left,right};
        }

    }
};
int main()
{
    Solution s1;
    vector<int>nums={5,7,7,8,8,10};
    s1.searchRange(nums,6);
    return 0;
}
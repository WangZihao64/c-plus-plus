#include<iostream>
//包含写和读文件
#include<fstream>
#include<string>
using namespace std;
int main()
{
    string file=R"(/Users/wangzihao/Cplusplus/file/test.txt)";
    // ofstream fout;  //创建文件输出流对象
    // fout.open("test.txt"); //打开文件，如果不存在则创建它。如果文件存在则截断其内容
    // ofstream fout(file,ios::app);
    
    
    //写文件
    // ofstream fout;
    // fout.open(file,ios::app);
    // fout << "hello c++" << endl;
    // fout << "hello wangzihao" << endl;
    // fout.close();

    //读文件
    //第一种方法
    ifstream fin;
    fin.open(file,ios::in);
    if(fin.is_open()==false)
    {
        cout << "文件打开失败" << endl;
    }
    string buffer; //用于存放从文件中读取的内容
    // while(getline(fin,buffer))
    // {
    //     cout << buffer << endl;
    // }

    cout << "文件操作完成" << endl; 

    //第二种方法
    while(fin >> buffer)
    {
        cout << buffer << endl;
    }
    return 0;
}
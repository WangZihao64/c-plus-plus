#include<iostream>
#include<string>
#include<unordered_map>
using namespace std;
//********错误写法！*********//

// void test(int a=10,int b=20,int c)
// {
//     std::cout << a << std::endl;
//     std::cout << b << std::endl;
//     std::cout << c << std::endl;
// }

//正确写法
// void test(int a,int b=20,int c=30)
// {
//     std::cout << a << std::endl;
//     std::cout << b << std::endl;
//     std::cout << c << std::endl;
// }
class Solution {
public:
    bool isIsomorphic(string s, string t) {
        unordered_map<char, char> s2t;
        unordered_map<char, char> t2s;
        int len = s.length();
        for (int i = 0; i < len; ++i) {
            char x = s[i], y = t[i];
            if ((s2t.count(x) && s2t[x] != y) || (t2s.count(y) && t2s[y] != x)) {
                return false;
            }
            s2t[x] = y;
            t2s[y] = x;
        }
        return true;
    }
};
int main()
{
	Solution s1;
    string str1="egg";
    string str2="add";
    bool ret=s1.isIsomorphic(str1,str2);
	return 0;
}
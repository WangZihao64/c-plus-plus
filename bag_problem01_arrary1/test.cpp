#include<iostream>
#include<vector>
using namespace std;
void test_bag_problem()
{
    vector<int> value={1,1,1};
    vector<int> weight={1,1,1};
    int bagweight=4;
    //背包容量为j,能背的最大物品价值
    vector<int> dp(bagweight+1);
    int i=0;
    int j=0;
    for(i=0;i<weight.size();i++)
    {
        for(j=bagweight;j-weight[i]>=0;j--)
        {
            dp[j]=max(dp[j],dp[j-weight[i]]+value[i]);
        }
    }
    cout << dp[bagweight] << endl;
}
int main()
{
    test_bag_problem();
    return 0;
}
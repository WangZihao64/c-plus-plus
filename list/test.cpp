#include<iostream>
#include<list>
using namespace std;
void PrintList(const list<int>& l1)
{
    for(list<int>::const_iterator it=l1.begin();it!=l1.end();it++)
    {
        cout << *it << endl;
    }
}
void test01()
{
    list<int> l1;
    l1.push_back(1);
    l1.push_back(2);
    PrintList(l1);
}
int main()
{
    test01();
    return 0;
}
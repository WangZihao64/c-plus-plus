// #include<iostream>
// #include<string>
// using namespace std;
// void test01()
// {
//     //基于范围的for循环
//     double prices[5]={1.1,2.2,3.3,4.4,5.5};
//     for(double x:prices)
//     {
//         cout << x << endl;
//     }
// }
// int main()
// {
//     //定义的几种方法
//     int a=10;
//     int b=(10);
//     int c(10);
//     cout << a << endl;
//     cout << b << endl;
//     cout << c << endl;
//     int d={10};
//     cout << d << endl;

//     //long long 类型
//     long long e=10;
//     cout << sizeof(e) << endl;

//     //原始字面变量
//     // string path="C:\\Program File\\Wangzihao";
//     // string path=R"path(C:\Program File\Wangzihao)path";
//     // cout << path << endl;

//     test01();
// }

#include<iostream>
#include<vector>
#include<unordered_set>
using namespace std;
// class Solution {
// private:
// bool backtracking(vector<vector<char>>& board) {
//     for (int i = 7; i < board.size(); i++) {        // 遍历行
//         for (int j = 7; j < board[0].size(); j++) { // 遍历列
//             if (board[i][j] == '.') {
//                 for (char k = '1'; k <= '9'; k++) {     // (i, j) 这个位置放k是否合适
//                     if (isValid(i, j, k, board)) {
//                         board[i][j] = k;                // 放置k
//                         if (backtracking(board))  // 如果找到合适一组立刻返回
//                         return true;
//                         board[i][j] = '.';              // 回溯，撤销k
//                     }
//                 }
//                 return false;  // 9个数都试完了，都不行，那么就返回false 
//             }                
//         }
//     }
//     return true; // 遍历完没有返回false，说明找到了合适棋盘位置了
// }
// bool isValid(int row, int col, char val, vector<vector<char>>& board) {
//     for (int i = 0; i < 9; i++) { // 判断行里是否重复
//         if (board[row][i] == val) {
//             return false;
//         }
//     }
//     for (int j = 0; j < 9; j++) { // 判断列里是否重复
//         if (board[j][col] == val) {
//             return false;
//         }
//     }
//     int startRow = (row / 3) * 3;
//     int startCol = (col / 3) * 3;
//     for (int i = startRow; i < startRow + 3; i++) { // 判断9方格里是否重复
//         for (int j = startCol; j < startCol + 3; j++) {
//             if (board[i][j] == val ) {
//                 return false;
//             }
//         }
//     }
//     return true;
// }
// public:
//     void solveSudoku(vector<vector<char>>& board) {
//         backtracking(board);
//     }
// };

// class Solution {
// public:
//     static bool cmp(const vector<int>& e1,const vector<int>& e2)
//     {
//         if(e1[0]==e2[0])
//         {
//             return e1[1]<e2[1];
//         }
//         return e1[0]<e2[0];
//     }
//     int eraseOverlapIntervals(vector<vector<int>>& intervals) {
//         //以X start从小到大排序
//         sort(intervals.begin(),intervals.end(),cmp);
//         int result=0;
//         for(int i=1;i<intervals.size();i++)
//         {
//             // printf("[%d %d ]",intervals[i][0],intervals[i-1][1]);
//             if(intervals[i][0]<intervals[i-1][1])
//             {
                
//                 result++;
//             }
//             //更改区间
//             else
//             {
//                 intervals[i][1]=min(intervals[i][1],intervals[i-1][1]);
//                 // printf("%d ",i);
//             }
//         }
//         return result;
//     }
// };

class Solution {
public:
    int monotoneIncreasingDigits(int n) {
        string str_num=to_string(n);
        for(int i=str_num.size()-2;i>=0;i--)
        {
            
            if(str_num[i]>str_num[i+1])
            {
                str_num[i]=str_num[i]-1;
                str_num[i+1]=9;
                cout<< str_num[i+1] << endl;
            }
        }
        
        return stoi(str_num);
    }
};

int main()
{
    Solution s1;
    int n=1234;
    vector<int> dp={1,2,3,4};
    dp.pop_back();
    s1.monotoneIncreasingDigits(n);
    return 0;
}
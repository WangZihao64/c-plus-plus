#include<iostream>
using namespace std;
template<typename T>//声明一个模版
void swap_(T& a,T& b)
{
    T tmp=a;
    a=b;
    b=tmp;
}
int main()
{
    int a=1;
    int b=2;
    swap_(a,b);
    printf("%d %d",a,b);
    return 0;
}
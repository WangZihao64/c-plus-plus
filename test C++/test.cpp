#include<iostream>
#include<string>
#include<vector>
using namespace std;
// struct TreeNode {
//     int val;
//     struct TreeNode *left;
//     struct TreeNode *right;
// };
// class Solution {
// int result=INT_MAX;
// TreeNode* prev=NULL;
// private:
//     void _getMinimumDifference(TreeNode* cur)
//     {
//         if(cur==NULL)
//         {
//             return;
//         }
//         //因为是搜索二叉树，所以中序即可排列成升序数组
//         _getMinimumDifference(cur->left);
//         if(prev!=NULL)
//         {
//             result=min(result,cur->val-prev->val);
//         }
//         //prev永远在cur的前一个
//         prev=cur;
//         _getMinimumDifference(cur->right);
//     }
// public:
//     int getMinimumDifference(TreeNode* root) {
//         _getMinimumDifference(root);
//         return result;
//     }
// };
// TreeNode* BuyBTNode(int x)
// {
//     TreeNode* newnode=new TreeNode();
//     newnode->val=x;
//     newnode->left=NULL;
//     newnode->right=NULL;
//     return newnode;
// }
// int main()
// {
//     TreeNode* root=BuyBTNode(4);
//     TreeNode* A1=BuyBTNode(2);
//     TreeNode* A2=BuyBTNode(6);
//     TreeNode* B1=BuyBTNode(1);
//     TreeNode* B2=BuyBTNode(3);
//     root->left=A1;
//     root->right=A2;
//     A1->left=B1;
//     A1->right=B2;
//     Solution s1;
//     s1.getMinimumDifference(root);
//     return 0;
// }
class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n) {
        //dp数组代表i个0 j个1可以放的最大子集
        vector<vector<int>> dp(m+1,vector<int>(n+1,0));
        int oneNum=0;
        int zeroNum=0;
        //遍历物品
        for(int i=0;i<strs.size();i++)
        {
            //取这一行的字符串
            string str=strs[i];
            for(int j=0;j<str.size();j++)
            {
                if(str[j]=='1')
                {
                    oneNum++;
                }
                else
                {
                    zeroNum++;
                }
            }
            //遍历背包
            for(int k=m;k>=zeroNum;k--)
            {
                for(int k1=n;k1>=oneNum;k1--)
                {
                    dp[k][k1]=max(dp[k][k1],dp[k-zeroNum][k-oneNum]+1);
                }
            }
        }
        return dp[m][n];
    }
};
int main()
{
    int n=10;
    vector<string> strs={"10","0001", "111001", "1", "0"};
    Solution s1;
    s1.findMaxForm(strs,5,3);
    return 0;
}
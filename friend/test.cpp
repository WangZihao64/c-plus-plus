#include<iostream>
#include<string>
using namespace std;
// class Person
// {
//     //声明友元，可以访问Person私有成员
//     //全局函数做友元
//     friend void test(Person* p1);
//     public:
//     //this指针是一个指针常量，不可以修改this的指向
//     //常函数
//     // void showPerson() const
//     // //const Person* const this
//     // {
//     //     m_age=100;
//     // }
//     //加了mutable就可以修改常函数了
//     Person()
//     {
//         m_age=18;
//         name="王子豪";
//     }
//     // mutable int m_age;
//     int m_age;
//     private:
//     string name;

// };
// void test(Person* p1)
// {
//     cout << p1->m_age << endl;
//     cout << p1->name << endl;
// }
//类做友元
// class Building;
// class GoodGay
// {
//     public:
//     GoodGay();
//     void visit(); //参观函数 访问building中的属性
//     Building* building;
// };
// class Building
// {
//     //goodgay是本类的好朋友，可以访问本类中的私有成员
//     friend class GoodGay;
//     public:
//     Building();
//     public:
//     string m_SittingRoom;
//     private:
//     string m_bedroom;
//     string m_toilet;
// };
// //类外泄成员函数
// Building::Building()
// {
//     m_SittingRoom = "客厅";
//     m_bedroom = "卧室";
//     m_toilet= "厕所";
// }
// GoodGay::GoodGay()
// {
//     building=new Building;
// }
// void GoodGay::visit()
// {
//      cout << "好基友类正在访问:" << building->m_SittingRoom << endl;
//      cout << "好基友类正在访问:" << building->m_bedroom << endl;
//      cout << "好基友类正在访问:" << building->m_toilet << endl;
// }
// void test01()
// {
//     GoodGay gg;
//     gg.visit();
// }
//成员函数做友元
class Building;
class GoodGay
{
    public:
    GoodGay();
    void vist1(); //vist1可以访问building的所有成员
    void vist2();
    Building* building;
};
class Building
{
    friend void GoodGay::vist1();
    public:
    Building();
    public:
    string m_SittingRom;
    private:
    string m_bedroom;
};
Building::Building()
{
    m_SittingRom="客厅";
    m_bedroom="卧室";
}
GoodGay::GoodGay()
{
    building=new Building;
}
void GoodGay::vist1()
{
    cout << "好基友正在访问：" << building->m_SittingRom << endl;
    cout << "好基友正在访问：" << building->m_bedroom << endl;
}
void GoodGay::vist2()
{
    cout << "好基友正在访问：" << building->m_SittingRom << endl;
}
void test02()
{
    GoodGay gg;
    gg.vist1();
}
int main()
{
    // Person p;
    // p.m_age=100;
    // Person p1;
    // test(&p1);
    //test01();
    test02();
    return 0;
}

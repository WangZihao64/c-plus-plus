#include<iostream>
#include<vector>
#include<string>
using namespace std;
class Person
{
    public:
    Person(string name,int age)
    {
        m_name=name;
        m_age=age;
    }
    public:
    string m_name;
    int m_age;
};
void test01()
{
    Person p1("wzh",22);
    Person p2("zq",22);
    Person p3("zmny",22);
    vector<Person> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    for(vector<Person>::iterator itBegin=v.begin();itBegin<v.end();itBegin++)
    {
        cout << " name is "<<(*itBegin).m_name << " ,age is " << (*itBegin).m_age << endl;
        // cout << " name is "<<itBegin->m_name << " ,age is " << itBegin->m_age << endl;
    }
}
void test02()
{
    Person p1("wzh",22);
    Person p2("zq",22);
    Person p3("zmny",22);
    vector<Person*> v;
    v.push_back(&p1);
    v.push_back(&p2);
    v.push_back(&p3);
    for(vector<Person*>::iterator itBegin=v.begin();itBegin<v.end();itBegin++)
    {
        cout << " name is "<<(*itBegin)->m_name << " ,age is " << (*itBegin)->m_age << endl;
        // cout << " name is "<<(*(*itBegin)).m_name << " ,age is " << (*(*itBegin)).m_age << endl;
    }
}
int main()
{
    //创建一个vector容器，数组
    vector<int> v;
    //向容器中插入数据
    v.push_back(10);
    v.push_back(20);
    v.push_back(30);
    v.push_back(40);
    //通过迭代器访问容器中的数据
    // vector<int>::iterator itBegin=v.begin();
    // vector<int>::iterator itEnd=v.end();
    // while(itBegin!=itEnd)
    // {
    //     cout << *itBegin <<endl;
    //     itBegin++;
    // }
    for(vector<int>::iterator itBegin=v.begin();itBegin<v.end();itBegin++)
    {
        cout << *itBegin << endl;
    }
    //test01();
    test02();
    return 0;  
}
//
// Created by 王子豪 on 2023/1/7.
//
#include <iostream>
#include <vector>
#include <math.h>
#include<queue>
#include <unordered_map>
using namespace std;
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
            : val(_val), left(_left), right(_right), next(_next) {}
};
//class Solution {
//public:
//    Node* connect(Node* root) {
//        if(root==NULL)
//        {
//            return root;
//        }
//        queue<Node*> q;
//        q.push(root);
//        while(!q.empty())
//        {
//            //计算这一层的大小
//            int n=q.size();
//            cout << n << " ";
//            for(int i=0;i<n;i++)
//            {
//                Node* node=q.front();
//                q.pop();
//                if(node->left!=NULL)
//                {
//                    q.push(root->left);
//                }
//                if(node->right!=NULL)
//                {
//                    q.push(root->right);
//                }
//                if(i==n-1)
//                {
//                    //这里每一层最后一个节点,最后一个节点应该指向空
//                    node->next=NULL;
//                    break;
//                }
//                Node* rightNode=q.front();
//                node->next=rightNode;
//            }
//
//        }
//        return root;
//    }
//};
//class Solution {
//public:
//    string sortSentence(string s) {
//        vector<string> arr(9);   // 单词数组
//        string tmp = "";   // 当前单词
//        int n = 0;   // 单词数量
//        // 遍历字符串
//        for (auto c: s){
//            if (c >= '0' && c <= '9'){
//                // 如果为数字，计算对应的单词数组下标，将单词放入对应位置，并清空当前单词
//                // 数组下标为 0 开头，位置索引为 1 开头
//                arr[c-'0'-1] = tmp;
//                tmp.clear();
//                ++n;
//            }
//            else if (c != ' '){
//                // 如果为字母，更新当前单词
//                tmp.push_back(c);
//            }
//        }
//        string res = arr[0];   // 原本顺序的句子
//        for (int i = 1; i < n; ++i){
//            res += " " + arr[i];
//        }
//        return res;
//    }
//};
//int main()
//{
//    Solution s1;
//    Node* root=new Node(1);
//    Node* A1=new Node(2);
//    Node* A2=new Node(3);
//    Node* B1=new Node(4);
//    Node* B2=new Node(5);
//    Node* B3=new Node(6);
//    Node* B4=new Node(7);
//    root->left=A1;
//    root->right=A2;
//    A1->left=B1;
//    A1->right=B2;
//    A2->left=B3;
//    A2->right=B4;
//    s1.connect(root);
//    return 0;
//}
//class Solution {
//public:
//    string sortSentence(string s) {
//        //记录单词的个数
//        int n=0;
//        //题目说了单词数目最多为9个，我们提前开好可以避免空间的消耗
//        vector<string> arr(9);
//        string word=" ";
//        for(int i=0;i<s.size();i++)
//        {
//            //如果是数字，找到对应的位置，插入到容器中
//            if(s[i]>='0'&&s[i]<='9')
//            {
//                //因为索引从1开始，所以我们必须要减1，数组下标从0开始
//                arr[s[i]-'0'-1]=word;
//                n++;
//                //重新放单词
//                word.clear();
//            }
//            else if(s[i]!=' ')
//            {
//                word+=s[i];
//            }
//        }
//        word.clear();
//        //放到字符串里
//        word=arr[0];
//        for(int i=1;i<n;i++)
//        {
//            //需要添加空格
//            word+=" "+arr[i];
//        }
//        return word;
//    }
//};
class Solution {
public:
    int findLHS(vector<int>& nums) {
        unordered_map<int, int> cnt;
        int res = 0;
        for (int num : nums) {
            cnt[num]++;
        }
        for (auto [key, val] : cnt) {
            if (cnt.count(key + 1)) {
                res = max(res, val + cnt[key + 1]);
            }
        }
        return res;
    }
};
int main()
{
    Solution s1;
    vector<int> nums={1,3,2,2,5,2,3,7};
    s1.findLHS(nums);
    return 0;
}
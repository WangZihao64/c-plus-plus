#include<iostream>
#include<deque>
#include<vector>
using namespace std;
void Print_Deque(const deque<int>& q1)
{
    for(deque<int>::const_iterator it=q1.begin();it!=q1.end();it++)
    {
        cout << *it << " ";
    }
    cout << endl;
}
void test01()
{
    deque<int> q1;
    int i=0;
    for(i=0;i<5;i++)
    {
        q1.push_back(i);
    }
    Print_Deque(q1);
    int cap=q1.__capacity();
    cout << cap << endl;
    int sz=q1.size();
    cout << sz << endl;
    
}
int main()
{
    test01();
    return 0;
}
//
// Created by 王子豪 on 2023/7/30.
//

#ifndef SMARTPTR_SMARTPTR_H
#define SMARTPTR_SMARTPTR_H

#endif //SMARTPTR_SMARTPTR_H
#include<iostream>
#include<mutex>
#include<memory>
using namespace std;
namespace wzh
{
    template<class T>
    class auto_ptr
    {
    public:
        //RAII
        auto_ptr(T* ptr)
        :_ptr(ptr)
        {}
        ~auto_ptr()
        {
            delete _ptr;
            //delete[] _ptr;
            cout << _ptr << endl;
        }
        //这里会造成悬空
        auto_ptr(auto_ptr<T>& sp)
        :_ptr(sp._ptr)
        {
            sp._ptr=nullptr;
        }
        //像指针一样
        T& operator*()
        {
            return *_ptr;
        }
        T* operator->()
        {
            return _ptr;
        }
        T& operator[](size_t pos)
        {
            return _ptr[pos];
        }
    private:
        T* _ptr;
    };
    template<class T>
    class unique_ptr
    {
    public:
        //RAII
        unique_ptr(T* ptr)
                :_ptr(ptr)
        {}
        ~unique_ptr()
        {
            delete _ptr;
            //delete[] _ptr;
            cout << _ptr << endl;
        }
        //防拷贝
        unique_ptr(const unique_ptr<T>& sp) = delete;
        //防构造
        unique_ptr<T>& operator=(const unique_ptr<T>& sp) = delete;
        //像指针一样
        T& operator*()
        {
            return *_ptr;
        }
        T* operator->()
        {
            return _ptr;
        }
        T& operator[](size_t pos)
        {
            return _ptr[pos];
        }
    private:
        T* _ptr;
    };
    template<class T>
    class default_delete
    {
    public:
        void operator()(T* ptr)
        {
            delete ptr;
        }
    };
    template<class T,class D=default_delete<T>>
    class shared_ptr
    {
    public:
        //RAII
        shared_ptr(T* ptr= nullptr)
                :_ptr(ptr)
                ,_pcount(new int(1))
                ,_pmutex(new mutex)
        {}
        ~shared_ptr()
        {
            Release();
        }
        //浅拷贝
        shared_ptr(const shared_ptr<T>& sp)
        :_ptr(sp._ptr)
        ,_pcount(sp._pcount)
        ,_pmutex(sp._pmutex)
        {
            AddRef();
        }
        //赋值重载很难写
        shared_ptr<T>& operator=(const shared_ptr<T>& sp)
        {
            if(_ptr!=sp._ptr)
            {
                Release();
                _pcount=sp._pcount;
                _ptr=sp._ptr;
                _pmutex=sp._pmutex;
                AddRef();
            }
            return *this;
        }
        void Release()
        {
            _pmutex->lock();
            bool flag=false;
            if(--(*_pcount)==0&&_ptr)
            {
                _del(_ptr);
                delete _pcount;
                flag=true;
            }
            _pmutex->unlock();
            if(flag==true)
            {
                delete _pmutex;
            }
        }
        void AddRef()
        {
            _pmutex->lock();
            ++(*_pcount);
            _pmutex->unlock();
        }

        //像指针一样
        T& operator*()
        {
            return *_ptr;
        }
        T* operator->()
        {
            return _ptr;
        }
        T& operator[](size_t pos)
        {
            return _ptr[pos];
        }
    public:
        T* _ptr;
        //因为是共享，所以必须使用指针的方式，普通定义不能共享
        int* _pcount;
        mutex* _pmutex;
        D _del;
    };
    template<class T>
    class weak_ptr
    {
    public:
        //RAII
        weak_ptr()
                :_ptr(nullptr)
        {}
        weak_ptr(const shared_ptr<T>& sp)
        {
            _ptr=sp.sp;
        }
        weak_ptr<T>& operator=(const wzh::shared_ptr<T>& sp)
        {
            _ptr=sp._ptr;
            return *this;
        }
        //像指针一样
        T& operator*()
        {
            return *_ptr;
        }
        T* operator->()
        {
            return _ptr;
        }
    private:
        T* _ptr;
    };
}
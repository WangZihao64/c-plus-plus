// #include<iostream>
// using namespace std;
// class Person
// {
//     public:
//     int m_a; //非静态成员的变量
//     static int m_b;
//     int age;
//     Person(int age)
//     {
//         //this可以解决名称冲突
//         this->age=age;
//     }
//     void func()
//     {

//     }
// };
// int Person::m_b=10;
// int main()
// {
//     // Person p;
//     // cout << sizeof(p) << endl;
//     Person p1(10);
//     cout << p1.age << endl;
//     return 0;
// }
#include <iostream>
 
using namespace std;
 
// 基类
class Shape 
{
   public:
      void setWidth(int w)
      {
         width = w;
      }
      void setHeight(int h)
      {
         height = h;
      }
   protected:
      int width;
      int height;
};
 
// 派生类
class Rectangle: public Shape
{
   public:
      int getArea()
      { 
         return (width * height); 
      }
};
 
int main(void)
{
   Rectangle Rect;
 
   Rect.setWidth(5);
   Rect.setHeight(7);
 
   // 输出对象的面积
   cout << "Total area: " << Rect.getArea() << endl;
 
   return 0;
}
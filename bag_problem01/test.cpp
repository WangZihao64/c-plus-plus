#include<iostream>
#include<vector>
using namespace std;
void test_arrary1()
{
    int i=0;
    int j=0;
    //物品价值
    vector<int> value={15,20,30};
    //物品重量
    vector<int> weight={1,3,4};
    //背包重量
    int bagweight=4;
    vector<int> bag(bagweight+1);
    //【0-i】个物品任意取，【j】代表背包容量 放进容量为j的背包里，价值容量最大总和是多少
    vector<vector<int>> dp(value.size(),bag);
    //dp数组的初始化
    for(i=0;i<value.size();i++)
    {
        dp[i][0]=0;
    }
    for(j=1;j<bag.size();j++)
    {
        dp[0][j]=value[0];
    }
    //遍历顺序，取得物品为行，背包重量为列
    for(i=1;i<value.size();i++)
    {
        for(j=1;j<=bagweight;j++)
        {
            //如果背包容量小于物品容量，就取正方上
            if(j<weight[i])
            {
                dp[i][j]=dp[i-1][j];
            }
            else
            {
                dp[i][j]=max(dp[i-1][j-weight[i]]+value[i],dp[i-1][j]);
            }
        }
    }
    cout << "可以带走的最大价值为:" << endl << dp[value.size()-1][bagweight] << endl;
}
int main()
{
    test_arrary1();
    return 0;
}
//
// Created by 王子豪 on 2023/1/6.
//

#include "test.h"
#include<iostream>
using namespace std;
//struct stack
//{
//    int* arr;
//    int top;
//    int capacity;
//};
//void stackInit(stack* st,int size=4)
//{
//    st->arr=new int(size);
//    st->top=0;
//    st->capacity=size;
//}
//int main()
//{
//    stack st;
//    stackInit(&st); //不知道要初始化为多大时这个缺省参数很方便，我们可以省略不写
//    stackInit(&st,100); //如果预先知道了要初始化为多大，可以自己设置缺省参数的值
//    return 0;
//}
int swap1(int& a,int& b)
{
    int tmp=a;
    a=b;
    b=tmp;
}
int main()
{
    int a=10;
    int b=20;
    swap(a,b);
}
